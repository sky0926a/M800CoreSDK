//
//  M800MessageSDK.h
//  M800MessageSDK
//
//  Created by Sarfaraz Khan on 12/8/2019.
//  Copyright © 2019 M800. All rights reserved.
//

#import <UIKit/UIKit.h>

//! Project version number for M800MessagingSDK.
FOUNDATION_EXPORT double M800MessagingSDKVersionNumber;

//! Project version string for M800MessagingSDK.
FOUNDATION_EXPORT const unsigned char M800MessagingSDKVersionString[];

// In this header, you should import all the public headers of your framework using statements like #import <M800MessagingSDK/PublicHeader.h>
#import <M800MessageSDK/M800MessageSDK.h>
