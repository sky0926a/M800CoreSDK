Pod::Spec.new do |spec|
  spec.name         = "webrtc-ios"
  spec.version      = "1.0.0"
  spec.summary      = "webrtc-ios is a framework for demo."
  spec.description  = <<-DESC
                    This is a demo framework for, do not use it!
                   DESC

  spec.homepage     = "https://gitlab.com/sky0926a/M800CoreSDK"

  spec.license      = { :type => "MIT", :file => "LICENSE" }

  spec.author       = { 'M800 Limited' => 'support@m800.com' }

  spec.platform     = :ios, "14.0"
  spec.swift_version = '5.7'
  spec.source       = { :git => "https://gitlab.com/sky0926a/M800CoreSDK.git", :tag => "#{spec.version}" }
  spec.ios.vendored_frameworks = 'WebRTC.xcframework'
  spec.dependency 'Starscream', '4.0.4'
  spec.dependency 'CocoaMQTT', '2.0.8'
  spec.user_target_xcconfig = { 'BUILD_LIBRARY_FOR_DISTRIBUTION' => 'YES' }
  spec.pod_target_xcconfig = { 'BUILD_LIBRARY_FOR_DISTRIBUTION' => 'YES' }


end
