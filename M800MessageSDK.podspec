Pod::Spec.new do |spec|
  spec.name         = "M800MessageSDK"
  spec.version      = "1.0.0"
  spec.summary      = "M800MessageSDK is a framework for demo."
  spec.description  = <<-DESC
                    This is a demo framework for, do not use it!
                   DESC

  spec.homepage     = "https://gitlab.com/sky0926a/M800CoreSDK"

  spec.license      = { :type => "MIT", :file => "LICENSE" }

  spec.author       = { 'M800 Limited' => 'support@m800.com' }

  spec.platform     = :ios, "14.0"
  spec.swift_version = '5.7'
  spec.source       = { :git => "https://gitlab.com/sky0926a/M800CoreSDK.git", :tag => "#{spec.version}" }
  spec.ios.vendored_frameworks = 'M800MessageSDK.xcframework'
  spec.dependency 'SwiftProtobuf', '1.8.0'
  spec.dependency 'SwiftSoup', '2.3.8'
  spec.user_target_xcconfig = { 'BUILD_LIBRARY_FOR_DISTRIBUTION' => 'YES' }
  spec.pod_target_xcconfig = { 'BUILD_LIBRARY_FOR_DISTRIBUTION' => 'YES' }


end
