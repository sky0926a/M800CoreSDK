Pod::Spec.new do |spec|
  spec.name         = "LiveConnectUIKit_iOS"
  spec.version      = "1.0.0"
  spec.summary      = "LiveConnectUIKit_iOS is a framework for demo."
  spec.description  = <<-DESC
                    This is a demo framework for, do not use it!
                   DESC

  spec.homepage     = "https://gitlab.com/sky0926a/M800CoreSDK"

  spec.license      = { :type => "MIT", :file => "LICENSE" }

  spec.author       = { 'M800 Limited' => 'support@m800.com' }

  spec.platform     = :ios, "14.0"
  spec.swift_version = '5.7'
  spec.source       = { :git => "https://gitlab.com/sky0926a/M800CoreSDK.git", :tag => "#{spec.version}" }
  spec.ios.vendored_frameworks = 'LiveConnectUIKit_iOS.xcframework'
  spec.dependency 'Zip', '~> 2.1.2'
  spec.dependency 'Cache', '5.2.0'
  spec.dependency 'UIImageView-Letters', '1.1.4'
  spec.dependency 'Kingfisher', '~> 7.6.2'
  spec.dependency 'SQLCipher', '~> 4.5.1'
  spec.user_target_xcconfig = { 'BUILD_LIBRARY_FOR_DISTRIBUTION' => 'YES' }
  spec.pod_target_xcconfig = { 'BUILD_LIBRARY_FOR_DISTRIBUTION' => 'YES' }


end
