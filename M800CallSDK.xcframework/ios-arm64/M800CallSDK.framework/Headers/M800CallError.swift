//
//  M800CallError.swift
//  M800CallSDK
//
//  Created by Hans Hsu on 2019/5/27.
//  Copyright © 2019 Hans Hsu. All rights reserved.
//

import Foundation
import M800CoreSDK

public enum M800CallError: Error, CustomStringConvertible, CinnoxErrorEquatable {
    case initializedFailed
    case connectFailed
    case callNotInReadyStep
    case callNotInConnectedStep
    case callNotInDisconnectedStep
    case callNotIncomingCall
    case callGetMySipaccountFailed
    case callNoServiceId
    case callNoSipAccount
    case callGetCalleeSipAccountFormatError
    case callNoCallIdResult
    case callOutgoingCallSwitchNotFound
    case callIncomingCallNotEstablishing
    case callNotTalking
    case callTokenExpired
    case callTokenInRefreshing
    case callDestoryed
    case callUnknown
    case callReachedMaximumSupportedCalls

    public var code: Int {
        switch self {
        case .initializedFailed:                    return 200
        case .connectFailed:                        return 202
        case .callNotInReadyStep:                   return 204
        case .callNotInConnectedStep:               return 205
        case .callNotInDisconnectedStep:            return 206
        case .callNotIncomingCall:                  return 208
        case .callGetMySipaccountFailed:            return 209
        case .callNoServiceId:                      return 212
        case .callNoSipAccount:                     return 213
        case .callGetCalleeSipAccountFormatError:   return 217
        case .callNoCallIdResult:                   return 218
        case .callOutgoingCallSwitchNotFound:       return 300
        case .callIncomingCallNotEstablishing:      return 400
        case .callNotTalking:                       return 500
        case .callTokenExpired:                     return 600
        case .callTokenInRefreshing:                return 601
        case .callReachedMaximumSupportedCalls:     return 602
        case .callDestoryed:                        return 998
        case .callUnknown:                          return 999

        default:
            return -1
        }
    }

    public var description: String {
        switch self {
        case .initializedFailed:                    return "Initialized failed"
        case .connectFailed:                        return "Connect failed"
        case .callNotInReadyStep:                   return "Call is not ready yet"
        case .callNotInConnectedStep:               return "Call is not connected yet"
        case .callNotInDisconnectedStep:            return "Call is not disconnected yet"
        case .callNotIncomingCall:                  return "Not an incoming call"
        case .callGetMySipaccountFailed:            return "Get sip account failed"
        case .callNoServiceId:                      return "No valid service Id"
        case .callNoSipAccount:                     return "No sip account"
        case .callGetCalleeSipAccountFormatError:   return "Sip account format error"
        case .callNoCallIdResult:                   return "No callId in prepare call response"
        case .callOutgoingCallSwitchNotFound:       return "Fail to switch on outgoing call"
        case .callIncomingCallNotEstablishing:      return "Incoming call is not established"
        case .callNotTalking:                       return "The call is not talking"
        case .callTokenExpired:                     return "Call token was expired"
        case .callTokenInRefreshing:                return "Call token is in-refreshing"
        case .callDestoryed:                        return "The call is destoryed"
        case .callUnknown:                          return "Unnown call error"
        case .callReachedMaximumSupportedCalls:     return "Call reached maximum supported calls"

        default:
            return "Undefined description yet"
        }
    }
}
