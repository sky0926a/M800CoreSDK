//
//  M800Trackable.swift
//  M800CallSDK
//
//  Created by Hans Hsu on 2020/5/13.
//  Copyright © 2020 m800. All rights reserved.
//

import UIKit
import WebRTC

protocol M800VideoInternalTrackDelegate: AnyObject {
    func trackDidChangeVideoSize(track: CinnoxCallVideoTrackable, videoSize: CGSize)
}

protocol M800VideoInternalTrackable: CinnoxCallVideoTrackable {
    func add(delegate: M800VideoInternalTrackDelegate)
    func remove(delegate: M800VideoInternalTrackDelegate)
}
