//
//  ZSSRichTextEditorViewController.h
//  ZSSRichTextEditor
//
//  Created by Nicholas Hubbard on 11/30/13.
//  Copyright (c) 2013 Zed Said Studio. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <WebKit/WebKit.h>


@class ZSSBarButtonItem;

/**
 *  The viewController used with ZSSRichTextEditor
 */
@interface ZSSRichTextEditor : UIViewController <WKUIDelegate, WKNavigationDelegate, WKScriptMessageHandler, UITextViewDelegate, UINavigationControllerDelegate>


/**
 *  The base URL to use for the webView
 */
@property (nonatomic, strong, nullable) NSURL *baseURL;

/**
 *  If the HTML should be formatted to be pretty
 */
@property (nonatomic) BOOL formatHTML;

/**
 * If the sub class recieves text did change events or not
 */
@property (nonatomic) BOOL receiveEditorDidChangeEvents;

/**
 *  The placeholder text to use if there is no editor content
 */
@property (nonatomic, strong, nullable) NSString *placeholder;

/**
 *  The placeholder text to use if there is no editor content
 */
@property (nonatomic, assign) CGFloat editorScrollOffset;

/**
 *  Sets the HTML for the entire editor
 *
 *  @param html  HTML string to set for the editor
 *
 */
- (void)setHTML:(NSString * _Nonnull)html;

/**
 *  Returns the HTML from the Rich Text Editor
 *
 */
- (void)getHTML:(void (^ _Nullable)(_Nullable id, NSError * _Nullable error))completionHandler;

/**
 *  Returns the plain text from the Rich Text Editor
 *
 */
- (void)getText:(void (^ _Nullable)(_Nullable id, NSError * _Nullable error))completionHandler;

/**
 *  Inserts HTML at the caret position
 *
 *  @param html  HTML string to insert
 *
 */
- (void)insertHTML:( NSString * _Nonnull)html;

/**
 *  Manually focuses on the text editor
 */
- (void)focusTextEditor;

/**
 *  Manually dismisses on the text editor
 */
- (void)blurTextEditor;

/**
 *  Inserts a link
 *
 *  @param url The URL for the link
 *  @param title The title for the link
 */
- (void)insertLink:(NSString * _Nonnull)url title:(NSString * _Nonnull)title;

/**
 *  Gets called when the insert URL picker button is tapped in an alertView
 *
 *  @warning The default implementation of this method is blank and does nothing
 */
- (void)showInsertURLAlternatePicker;

/**
 *  Gets called when the insert Image picker button is tapped in an alertView
 *
 *  @warning The default implementation of this method is blank and does nothing
 */
- (void)showInsertImageAlternatePicker;

/**
 *  Scroll event callback with position
 */
- (void)editorDidScrollWithPosition:(CGFloat)position;

/**
 *  Text change callback with text and html
 */
- (void)editorDidChangeWithText:(NSString * _Nonnull)text andHTML:(NSString * _Nonnull)html andHeight: (CGFloat) height;

/**
 *  Hashtag callback with word
 */
- (void)hashtagRecognizedWithWord:(NSString * _Nullable)word;

/**
 *  Mention callback with word
 */
- (void)mentionRecognizedWithWord:(NSString * _Nullable)word;

/**
 *  Set custom css
 */
- (void)setCSS:(NSString * _Nonnull)css;

/**
 *  Set tool bar hidden
 */
- (CGFloat)getEditorViewHeight;

- (void)alignLeft;
- (void)alignCenter;
- (void)alignRight;
- (void)setBold;
- (void)setItalic;
- (void)setUnderline;
- (void)setStrikethrough;
- (void)setUnorderedList;
- (void)setOrderedList;
- (void)heading2;
- (void)heading3;
- (void)paragraph;

- (void)updateToolBarItems:(NSArray<NSString *> *_Nonnull)items;

@end
