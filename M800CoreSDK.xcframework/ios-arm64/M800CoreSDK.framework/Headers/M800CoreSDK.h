//
//  M800CoreSDK.h
//  M800CoreSDK
//
//  Created by James Hung on 2019/3/22.
//  Copyright © 2019 m800. All rights reserved.
//

#import <UIKit/UIKit.h>

//! Project version number for M800CoreSDK.
FOUNDATION_EXPORT double M800CoreSDKVersionNumber;

//! Project version string for M800CoreSDK.
FOUNDATION_EXPORT const unsigned char M800CoreSDKVersionString[];

// In this header, you should import all the public headers of your framework using statements like #import <M800CoreSDK/PublicHeader.h>
#import "XXTea.h"

